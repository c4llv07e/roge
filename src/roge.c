#include <roge.h>

any
roge_end()
{
  printf("Thanks for playing roge\n");
  return (any)NOERR;
}

any
roge_startup()
{
  init_all();

  game_start();

  startup();
  loop();

  deinit_all();
  roge_end();
  return (any)NOERR;
}
