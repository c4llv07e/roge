#include <roge.h>

WINDOW* window_game;
WINDOW* window_log;
// WINDOW* window_inventory;

any
get_window_game()
{
  return window_game;
}

any
get_window_log()
{
  return window_log;
}

int
get_game_width()
{
  return getmaxx(window_game);
}

int
get_game_height()
{
  return getmaxy(window_game);
}

any
init_curs()
{
  initscr();
  curs_set(0);
  raw();
  cbreak();
  nodelay(stdscr, true);
  noecho();
  keypad(stdscr, true);

  window_game = newwin(20, 40, 0, 0);
  window_log  = newwin(3, 40, 20, 0);

  return (any)NOERR;
}

any
deinit_curs()
{

  delwin(window_game);
  delwin(window_log);
  endwin();
  return (any)NOERR;
}
