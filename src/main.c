#include <roge.h>

int
version()
{
  printf("version: %d.%d\n", GAME_VERSION, GAME_SUB_VERSION);
  return 0;
}

int
help()
{
  printf(
    "roge version %d.%d\n"
    "\n"
    "  -h	- Help\n", 
    GAME_VERSION, GAME_SUB_VERSION);
  return 0;
}

int
main(int argv, char** args)
{

  if (argv >= 2)
  {
    if (strcmp(args[1], "-v") == 0)
      return version();
    else if (strcmp(args[1], "-h") == 0)
      return help();
  }
  if (roge_startup() != NOERR)
    return -1;
  return 0;
}
