#include <roge.h>

struct object_array* objects;

any
init_object()
{
  objects = (struct object_array*)malloc(sizeof(struct object_array));
  objects->objects = (struct object**)malloc(1024*sizeof(struct object*));
  objects->len = 0;

  return (any)NOERR;
}

any
get_objects()
{
  return objects;
}

any
add_object(any _object)
{
  if (_object == NULL)
    return (any)ERROR_NULL;

  struct object* object = (struct object*) _object;

  objects->objects[objects->len++] = object;
  
  return (any)NOERR;
}


any
deinit_object()
{
  free(objects);

  return (any)NOERR;
}
