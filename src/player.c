#include <roge.h>

any
create_player(int _x, int _y)
{
  void* _player = malloc(sizeof(struct object));
  if (_player == NULL)
    return (any)ERROR_MEMORY;

  struct object* player = (struct object*) _player;

  player->x = _x;
  player->y = _y;
  player->ch = '@';

  return (any)player;
}

any
player_move(any _player, int _x, int _y)
{
  if (_player == NULL)
    return (any)ERROR_NULL;

  struct object* player = (struct object*) _player;

  player->x += _x;
  player->y += _y;

  return (any)NOERR;
}
