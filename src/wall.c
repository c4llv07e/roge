#include <roge.h>

any
create_wall(int _x, int _y)
{
  void* _wall = malloc(sizeof(struct object));

  if (_wall == NULL)
    return (any)ERROR_MEMORY;

  struct object* wall = (struct object*) _wall;

  wall->x = _x;
  wall->y = _y;
  wall->ch = '#';

  return wall;
}
