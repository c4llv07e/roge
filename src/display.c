#include <roge.h>

any centered_object;

any
init_display()
{
  return (any)NOERR;
}

any
draw_display()
{
  any _win_game = get_window_game();
  any _win_log  = get_window_log();

  if (_win_game == NULL)
    return (any)ERROR_NULL;

  if (_win_log == NULL)
    return (any)ERROR_NULL;

  WINDOW* win_game = (WINDOW*) _win_game;
  WINDOW* win_log = (WINDOW*) _win_log;

  box(win_game, 0, 0);
  box(win_log, 0, 0);

  return (any)NOERR;
}

any
deinit_display()
{
  return (any)NOERR;
}

any
set_centered_object(any _object)
{
  centered_object = _object;
  return (any)NOERR;
}

any
display_object(any _object)
{
  if (_object == NULL)
    return (any)ERROR_NULL;

  struct object* object = (struct object*) _object;
  mvwaddch(
      get_window_game(),
      object->y,
      object->x,
      object->ch
      );
  return (any)NOERR;
}

any
display_object_offset(any _object, int _x, int _y)
{
  if (_object == NULL)
    return (any)ERROR_NULL;

  struct object* object = (struct object*) _object;
  mvwaddch(
      get_window_game(),
      object->y-_y,
      object->x-_x,
      object->ch
      );
  return (any)NOERR;
}

any
display_redraw()
{
  wrefresh(get_window_game());
  wrefresh(get_window_log());
  refresh();

  return (any)NOERR;
}

any
display_all()
{
  any _objects = get_objects();

  if (_objects == NULL)
    return (any)ERROR_NULL;

  struct object_array* objects = (struct object_array*) _objects;

  display_list((any)objects);
  return (any)NOERR;
}

any
display_list(any _objects)
{
  if (_objects == NULL)
    return (any)ERROR_NULL;

  struct object_array* objects = (struct object_array*) _objects;
  int offset_x = 0, offset_y = 0;

  if (centered_object != NULL)
  {
    struct object* center = (struct object*) centered_object;
    offset_x = center->x - get_game_width()/2;
    offset_y = center->y - get_game_height()/2;
  }

  for (uint_t i = 0; i < objects->len; i++)
  {
    struct object* object = (struct object*)objects->objects[i];
    display_object_offset(object, offset_x, offset_y);
  }
  
  return (any)NOERR;
}

any
display_clear()
{
  werase(get_window_game());
  werase(get_window_log());
  return (any)NOERR;
}

